<?php
require_once __DIR__ . "/../vendor/autoload.php";
use demo001\hello\HelloWorld;

$obj = new HelloWorld();
echo $obj->hello();

echo "\n";
$obj = new HelloWorld('My Goddess');
echo $obj->hello();